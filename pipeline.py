from preprocess import preprocess
from train import train
from evaluate import evaluate
import kfp

def pipeline(
        url = "https://gist.githubusercontent.com/netj/8836201/raw/6f9306ad21398ea43cba4f7d537619d0e07d5ae3/iris.csv",
        target_column: str = "variety", 
        max_depth: int = 1
    ):

    ################# Definicje podzadan ############################
    web_downloader_op = kfp.components.load_component_from_url(
        'https://raw.githubusercontent.com/kubeflow/pipelines/master/components/web/Download/component.yaml')

    preprocess_op = kfp.components.create_component_from_func(
        func=preprocess,
        base_image='python:3.7',
        packages_to_install=['numpy', 'pandas', 'scikit-learn']
    )

    train_op = kfp.components.create_component_from_func(
        func=train,
        base_image='python:3.7',
        packages_to_install=['numpy', 'pandas', 'scikit-learn==0.23.2']
    )

    evaluate_op = kfp.components.create_component_from_func(
        func=evaluate,
        base_image='python:3.7',
        packages_to_install=['numpy', 'pandas', 'scikit-learn==0.23.2']
    )
    ##############################################################

    # Wykonanie pipeline-u
    web_downloader_task = web_downloader_op(url=url)

    preprocess_task = preprocess_op(dataset=web_downloader_task.outputs['data'])

    train_task = train_op(
        train_dataset=preprocess_task.outputs['out_train'],
        test_dataset=preprocess_task.outputs['out_test'],
        target_column=target_column,
        max_depth=max_depth
    )

    evaluate_task = evaluate_op(
        predictions=train_task.outputs['predictions'],
        reference=preprocess_task.outputs['out_test'],
        target_column=target_column
    )

if __name__ == '__main__':
    kfp.compiler.Compiler().compile(
    pipeline_func=pipeline,
    package_path='pipeline.yaml')
