import kfp.components as comp

# if __name__ == '__main__':
#    parser = argparse.ArgumentParser()
#    parser.add_argument("--dataset", type=str, required=True, help="Path to train dataset")
#    parser.add_argument("--out_train", type=str, required=True, help="Column with classname")
#    parser.add_argument("--out_test", type=str, required=True, help="Column with classname")

def preprocess(dataset: comp.InputPath("CSV"), out_train: comp.OutputPath("CSV"), out_test: comp.OutputPath("CSV")):
    import pandas as pd
    import numpy as np

    df = pd.read_csv(dataset)
    train, test = np.split(df.sample(frac=1), [int(.6*len(df))])

    train.to_csv(out_train, index=False)
    test.to_csv(out_test, index=False)