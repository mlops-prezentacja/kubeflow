FROM python3.7
RUN python3 -m pip pandas numpy scikit-learn
COPY train.py evaluate.py preprocess.py